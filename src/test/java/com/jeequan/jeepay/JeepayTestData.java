package com.jeequan.jeepay;

public class JeepayTestData {

    public static String getApiBase() {
        return "https://apptest.ahetc.com.cn:13008/payment";
    }

    public static String getApiKey() {
        return "txzo99xtpci4pk6mlaurw6mnd84u7e3lpqu1vlq4pzgdzkjh4uf35kjy3sm5lumk5bu1w3kjcifqht0u99onw7m9bkjt3c8esmp4mu0d28cy7y4y32jtibg3izx3rma3";
    }

    public static String getMchNo() {
        return "M1661239017";
    }

    public static String getAppId() { return "63047eea96b89730ded1eeaa";}

}
