package com.jeequan.jeepay.constants;

/**
 * 常量
 *
 * @author Kevin Ma
 */
public interface JeepayConstants {
    /**
     * 订单状态
     */
    interface OrderState {
        /**
         * 订单生成
         */
        Integer Created = 0;
        /**
         * 支付中
         */
        Integer Paying = 1;
        /**
         * 支付成功
         */
        Integer Success = 2;
        /**
         * 支付失败
         */
        Integer Failure = 3;
        /**
         * 已撤销
         */
        Integer Canceled = 4;
        /**
         * 已退款(全额)
         */
        Integer Refunded = 5;
        /**
         * 订单关闭
         */
        Integer Closed = 6;
    }

    /**
     * 支付数据类型
     */
    interface PayDateType {
        /**
         * 跳转链接的方式
         */
        String PayUrl = "payUrl";
        /**
         * 表单方式
         */
        String Form = "form";
        /**
         * 微信支付参数（微信公众号、小程序、app支付时）
         */
        String WX_APP = "wxapp";
        /**
         * 支付宝app支付参数
         */
        String ALI_APP = "aliapp";
        /**
         * 云闪付app支付参数
         */
        String YSF_APP = "ysfapp";
        /**
         * 二维码地址
         */
        String CodeUrl = "codeUrl";
        /**
         * 二维码图片地址
         */
        String CodeImgUrl = "codeImgUrl";
        /**
         * 空支付参数
         **/
        String None = "none";
        /**
         * 银联APP支付参数
         */
        String UNION_APP = "unionapp";
    }

    /**
     * 退款状态
     */
    interface RefundState {
        /**
         * 订单生成
         */
        Integer Created = 0;
        /**
         * 退款中
         */
        Integer Refunding = 1;
        /**
         * 退款成功
         */
        Integer Success = 2;
        /**
         * 退款失败
         */
        Integer Failure = 3;
        /**
         * 退款关闭
         */
        Integer Closed = 4;
    }

    /**
     * 签名方式，目前只支持MD5
     */
    interface SignType {
        String MD5 = "MD5";
    }

    /**
     * 支付接口，支付渠道
     */
    interface IfCode {
        /** 支付宝 */
        String Alipay = "alipay";
        /** 微信 */
        String Wxpay = "wxpay";
        /** 银联支付 */
        String Unionpay = "unionpay";
        /** 云闪付 **/
        String Ysfpay = "ysfpay";
        /** PayPal 支付*/
        String PayPal = "pppay";
    }
    /**
     * 支付方式
     */
    interface WayCode {
        String ALI_APP = "ALI_APP"; // 支付宝APP
        String ALI_BAR = "ALI_BAR"; // 支付宝条码
        String ALI_JSAPI = "ALI_JSAPI"; // 支付宝生活号
        String ALI_LITE = "ALI_LITE"; // 支付宝小程序
        String ALI_PC = "ALI_PC"; // 支付宝PC网站
        String ALI_QR = "ALI_QR"; // 支付宝二维码
        String ALI_WAP = "ALI_WAP"; // 支付宝WAP
        String PP_PC = "PP_PC"; // PayPal支付
        String UNION_APP = "UNION_APP"; // 银联APP支付
        String UNION_JSAPI = "UNION_JSAPI"; // 银联JS支付
        String UNION_WAP = "UNION_WAP"; // 银联手机网站支付
        String WX_APP = "WX_APP"; // 微信APP
        String WX_BAR = "WX_BAR"; // 微信条码
        String WX_H5 = "WX_H5"; // 微信H5
        String WX_JSAPI = "WX_JSAPI"; // 微信公众号
        String WX_LITE = "WX_LITE"; // 微信小程序
        String WX_NATIVE = "WX_NATIVE"; // 微信扫码
        String YSF_BAR = "YSF_BAR"; // 云闪付条码
        String YSF_JSAPI = "YSF_JSAPI"; // 云闪付jsapi
        String ICBC_AGGREGATE_PAY = "ICBC_AGGREGATE_PAY"; // 工行微信公众号聚合支付
        String ICBC_QR = "ICBC_QR"; // 工行二维码支付
        String ICBC_SCENE_PAY = "ICBC_SCENE_PAY"; // 工行场景支付
    }


}
